package cz.vse.java.dvop10.adventurasem.logika;


import java.util.HashSet;
import java.util.Set;

/**
 *  Class HerniPlan - třída představující mapu a stav adventury.
 * 
 *  Tato třída inicializuje prvky ze kterých se hra skládá:
 *  vytváří všechny prostory,
 *  propojuje je vzájemně pomocí východů 
 *  a pamatuje si aktuální prostor, ve kterém se hráč právě nachází.
 *
 *@author     Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova, Alena Buchalcevova
 *@version    z kurzu 4IT101 pro školní rok 2014/2015
 */
public class HerniPlan {
    
    private Prostor aktualniProstor;
    private Prostor viteznyProstor;

    private Set seznamPozorovatelu = new HashSet<>();
    
     /**
     *  Konstruktor který vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví halu.
     */
    public HerniPlan() {
        zalozProstoryHry();

    }
    /**
     *  Vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví domeček.
     */
    private void zalozProstoryHry() {
        // vytvářejí se jednotlivé prostory
        Prostor louka = new Prostor("louka","vyprahlá louka"); // 250 30
        Prostor vesnice = new Prostor("vesnice", "vesnice s pár vesničanama");
        Prostor hospoda = new Prostor("hospoda","hospoda smrdící chlastem");
        Prostor udoli = new Prostor("udoli","udoli");
        Prostor jeskyne = new Prostor("jeskyne","jeskyne kde žije drak");
        Prostor kostel = new Prostor("kostel","posvátný kostel");
        Prostor temny_les = new Prostor("temny_les","temny_les");
        Prostor trh = new Prostor("trh","prázdný trh");
        
        // přiřazují se průchody mezi prostory (sousedící prostory)
        louka.setVychod(vesnice);
        vesnice.setVychod(hospoda);

        vesnice.setVychod(kostel);
        vesnice.setVychod(trh);
        vesnice.setVychod(udoli);
        vesnice.setVychod(louka);
        hospoda.setVychod(vesnice);

        kostel.setVychod(vesnice);
        trh.setVychod(vesnice);
        udoli.setVychod(vesnice);
        udoli.setVychod(temny_les);
        temny_les.setVychod(jeskyne);
        temny_les.setVychod(udoli);
        jeskyne.setVychod(udoli);
                
        aktualniProstor = louka;  // hra začíná v domečku
        viteznyProstor = jeskyne ;

        louka.vlozVec(new Vec("kamen" ,true));
        louka.vlozVec(new Vec("zvlastni_kyticka" ,true));
        louka.vlozVec(new Vec("mec" ,true));
        louka.vlozVec(new Vec("strom" ,false));

        vesnice.vlozVec(new Vec("kamen" ,true));
        vesnice.vlozVec(new Vec("pes" ,false));
        vesnice.vlozVec(new Vec("Vesnican" ,false));

        hospoda.vlozVec(new Vec("stul",false));
        hospoda.vlozVec(new Vec("zidle",false));
        hospoda.vlozVec(new Vec("hospodsky",false));
        hospoda.vlozVec(new Vec("stamgast",false));

        udoli.vlozVec(new Vec("znacka",false));

        jeskyne.vlozVec(new Vec("drak", false));
        jeskyne.vlozVec(new Vec("poklad", false));
        jeskyne.vlozVec(new Vec("kamen", true));


        kostel.vlozVec(new Vec("obraz",true));
        kostel.vlozVec(new Vec("kropenka",true));

        trh.vlozVec(new Vec("trhar",false));
        trh.vlozVec(new Vec("rajce",true));
        trh.vlozVec(new Vec("jablko",true));

        temny_les.vlozVec(new Vec("kamen",true));


    }
    
    /**
     *  Metoda vrací odkaz na aktuální prostor, ve ktetém se hráč právě nachází.
     *
     *@return     aktuální prostor
     */
    
    public Prostor getAktualniProstor() {
        return aktualniProstor;
    }
    
    /**
     *  Metoda nastaví aktuální prostor, používá se nejčastěji při přechodu mezi prostory
     *
     *@param  prostor nový aktuální prostor
     */
    public void setAktualniProstor(Prostor prostor) {
       aktualniProstor = prostor;
    }
    /**
     *  Metoda vrací odkaz na vítězný prostor.
     *
     *@return     vítězný prostor
     */
    
    public Prostor getViteznyProstor() {
        return viteznyProstor;
    }

}
